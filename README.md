# Trainees 2022

**Exercício**

A tarefa consiste em uma simples apresentação dentro de uma página HTML, para fazer ela siga os seguintes passos se baseando no que você aprendeu nos conteúdos sobre Git, GitLab e Code Review

- Baixe o repositório na sua máquina
- Vamos fazer as alterações em um arquivo em conjunto, para não atrapalhar o de nenhum outro membro, crie uma branch nova
- A partir dessa branch, altere o arquivo index.html, copiando o código de template e adicionando um logo abaixo com as suas informações
- Crie uma versão dessas alterações
- Envie suas alterações para o GitLab
- Você precisa enviar suas alterações para o resto do time, para isso crie um Merge Request para a branch estável do projeto.
- Seu código será revisado pelo time, aproveite para revisar e aprovar os Merge Request do resto do grupo
- Quando for aprovado por pelo menos 2 desenvolvedores, faça o merge desse Merge Request e delete a sua branch.

